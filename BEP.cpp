// Autorzy:
// moi
// studenci

// BEP - Break Ivent Point
// Program do okreslania progu rentownosci przedsiebiortwa
// Uwzglednia produkcje jednoasortymentowa i wieloasortymentowa

#include <stdlib.h>	// obsluga printf, scanf, operacje arytmetyczne
#include <stdio.h>  // obsluga printf, scanf, operacje arytmetyczne
#include <conio.h>  // obsluga clrscr, kbhit, getch
#include <vector>

// printf - drukuje cos na ekranie
// kbhit  - sprawdza czy klawisz zostal wcisniety
// getch  - pobiera znak i przypisuje wartosc do (key)
// switch - sprawdze wartosc (key) i wybiera case
// float  - deklaracje zmiennych zmiennoprzecinkowych
// scanf  - wczytuje zmienna z klawiatury
// %f     - formatowanie na liczbe zmiennoprzecinkowa
// %d     - formatowanie na liczbe calkowita

class Asortyment
{
public:
	float IloscW;
	float IloscS;
	float Cena;
	float KosztyJednostkowe;
};

int main()
{
	char key = 1;	                           // definicja zmiennej

	while (key != 27)	                       // dopoki key rozne od 27 (klawisz ESC)
	{
		if (key)                           // jesli key rozne od zera to wypisz na ekranie menu glowne
		{
			system("cls");                      // wyczysc ekran
			printf("BEP - break event point\n\n");
			printf("Wybierz wariant:\n\n1. Produkcja jedno-asortymentowa.\n2. Produkcja wielo-asortymentowa.");
			printf("\n\nWci�nij 1, 2 lub ESC (wyj�cie)");
		}
		key = 0;
		if (_kbhit()) key = _getch();          // jesli nacisnieto przycisk odczytaj kod znaku

		switch (key)                       // instrukcja switch - przelacznik, wybiera wariant
		{
			case '1':                      // dla produkcji jednoasortymentowej

				float ks, kjz, c;           // deklaracje zmiennych							

				system("cls");	               // wyczysc ekran
				printf("Produkcja jedno-asortymentowa\n\n");
				printf("Wybierz wariant:\n\n1. Ilo�ciowy.\n2. Warto�ciowy.");
				printf("\n\nWci�nij 1, 2 lub ESC (wyj�cie)");

				while (!_kbhit());
				key = _getch();

				system("cls");

				switch (key)
				{
				case '1':              // produkcja jednosortymentowa w wyrazeniu ilosciowym
					printf("Produkcja jedno-asortymentowa w wyra�eniu ilo�ciowym\n\n");
					printf("Sta�e koszty produkcji: ");
					scanf("%f", &ks);	   // wczytuje zmienna (koszty stale)
					printf("Jednostkowe koszty zmienne produkcji: ");
					scanf("%f", &kjz);	   // wczytuje zmienna (jednostkowe koszty zmienne)
					printf("Jednostkowa cena sprzeda�y: ");
					scanf("%f", &c);        // wczytuje zmienna (cena sprzedazy)
					if (c - kjz)
					{
						printf("\nBEPi = %0.3f", (ks) / (c - kjz));
						printf("\n\nDowolny klawisz...");
						_getch();           //czekaj na klawisz
					}
					else
					{
						printf("\nNie mo�na obliczy�...B��d dzielenia przez zero...\nDowolny klawisz...\n");
						printf("\n\nDowlony klawisz...");
						_getch();           //czekaj na klawisz
					}
					break;

					//==============================================================

				case '2':
					printf("Produkcja jedno-asortymentowa w wyra�eniu warto�ciowym\n\n");
					printf("Sta�e koszty produkcji: ");
					scanf("%f", &ks);	   //wczytuje zmienna ks (koszty stale)
					printf("Jednostkowe koszty zmienne produkcji: ");
					scanf("%f", &kjz);	   //wczytuje zmienna (jednostkowe koszty zmienne)
					printf("Jednostkowa cena sprzeda�y: ");
					scanf("%f", &c);	       //wczytuje zmienna (cena sprzedazy)
					if (c - kjz)
					{
						printf("\nBEPw = %0.3f", (ks * c) / (c - kjz));
						printf("\n\nDowolny klawisz...");
						_getch();           //czekaj na klawisz
					}
					else
					{
						printf("\nNie mo�na obliczy�...B��d dzielenia przez zero...\nDowolny klawisz...\n");
						printf("\n\nDowolny klawisz...");
						_getch();           //czekaj na klawisz
					}
					break;
				}
				break;

				//==============================================================

			case '2':                      //dla produkcji wieloasortymentowej
			{
				float i, ks;
				std::vector<Asortyment> asortymenty;

				printf("\n\nProdukcja wielo-asortymentowa w wyrazeniu ilosciowym\n\n");
				printf("Stale koszty produkcji: ");
				scanf_s("%f", &ks);	   // wczytuje zmienna (koszty stale)
				printf("Ilosc wytwarznaych asortymentow wyrobu: ");
				scanf_s("%f", &i);
				for (int a = 0; a < i; ++a)
				{
					Asortyment as;
					asortymenty.push_back(as);
				}				
				printf("Liczba sprzedawanych wyrobow i-tego asortymentu\n");
				for (int a = 0; a < i; ++a)
				{
					scanf_s("%f", &asortymenty[a].IloscS);
				}
				printf("Koszty jednostkowe i-tego asortymentu: \n");
				for (int a = 0; a < i; ++a)
				{
					scanf_s("%f", &asortymenty[a].KosztyJednostkowe);
				}
				printf("Jednostkowa cena sprzedazy i-tego asortymentu: \n");
				for (int a = 0; a < i; ++a)
				{
					scanf_s("%f", &asortymenty[a].Cena);
				}

				float sumaPk = 0;
				float sumaPc = 0;
				float sumaC = 0;

				for (int a = 0; a < asortymenty.size(); ++a)
				{
					sumaPk += asortymenty[a].IloscS * asortymenty[a].KosztyJednostkowe;
					sumaPc += asortymenty[a].IloscS * asortymenty[a].Cena;
					sumaC += asortymenty[a].Cena;
				}
				//printf("%f %f %f", sumaPk, sumaPc, sumaC);
				float bepw = (ks / (1 - (sumaPk / sumaPc))) * sumaC;
				float bepi = (ks / (1 - (sumaPk / sumaPc)));

				printf("W wyrazeniu wartosciowym: %f\n", bepw);
				printf("W wyrazeniu ilosciowym: %f", bepi);
				_getch();
				break;
			}
		}
	}
}